class Backend::SlidersController < Backend::BaseController
  authorize_resource
  before_action :find_slider, only: [:edit, :update, :destroy]

  def index
    @sliders = Slider.page(params[:page]).per(30)
    @breadcrumbs = {'Sliders' => admin_sliders_path, t('view.layout.list') => nil}
  end

  def new
    @breadcrumbs = {'Sliders' => admin_sliders_path, t('view.button.new') => nil}
    @slider = Slider.new
  end

  def edit
    @breadcrumbs = {'Sliders' => admin_sliders_path, t('view.button.edit') => nil}
  end

  def create
    @slider = Slider.new slider_params
    if @slider.save
      respond_to do |format|
        format.html {redirect_to admin_sliders_path, notice: t('view.messages.created_success', model_name: 'Slider')}
      end
    else
      @breadcrumbs = {"Slider" => admin_sliders_path, t('view.button.new') => nil}
      render :new
    end
  end

  def update
    if @slider.update_attributes slider_params
      respond_to do |format|
        format.html {redirect_to admin_sliders_path, notice: t('view.messages.updated_success', model_name: 'Slider')}
      end
    else
      @breadcrumbs = {'Slider' => admin_sliders_path, t('view.button.edit') => nil}
      render :edit
    end
  end

  def destroy
    if @slider.destroy
      flash[:success] = t('view.messages.deleted_success', model_name: 'Slider')
    else
      flash[:danger] = @slider.errors.full_messages
    end

    redirect_to admin_sliders_path
  end

  private

  def find_slider
    @slider = Slider.find_by_id params[:id]
    redirect_to admin_sliders_path, flash: {danger: t('view.messages.not_found')} unless @slider
  end
  def slider_params
    params.require(:slider).permit :image, :status
  end
end