class Backend::ProductsController < Backend::BaseController
  authorize_resource
  before_action :find_product, only: [:edit, :update, :destroy]

  def index
    @products = Product.active.page(params[:page]).per(30)
    @breadcrumbs = {'list_products' => admin_products_path, t('view.layout.list') => nil}
  end

  def new
    @breadcrumbs = {'list_products' => admin_products_path, t('view.button.new') => nil}
    @categories = Category.active.child_categories
    @product = Product.new
    4.times do 
      @product.product_images.build
    end
  end

  def edit
    @breadcrumbs = {'list_products' => admin_products_path, t('view.button.edit') => nil}
    @categories = Category.active
    @product.product_images.build unless @product.product_images.present?
  end

  def create
    @product = Product.new product_params
    if @product.save
      respond_to do |format|
        format.html {redirect_to admin_products_path, notice: t('view.messages.created_success', model_name: 'Product')}
      end
    else
      @breadcrumbs = {t('view.managers.managers') => admin_products_path, t('view.button.new') => nil}
      render :new
    end
  end

  def update
    if @product.update_attributes product_params
      respond_to do |format|
        format.html {redirect_to admin_products_path, notice: t('view.messages.updated_success', model_name: 'Product')}
      end
    else
      @breadcrumbs = {'list_products' => admin_products_path, t('view.button.edit') => nil}
      render :edit
    end
  end

  def destroy
    if @product.destroy
      flash[:success] = t('view.messages.deleted_success', model_name: 'Product')
    else
      flash[:danger] = @product.errors.full_messages
    end

    redirect_to admin_products_path
  end

  private

  def find_product
    @product = Product.find_by_id params[:id]
    redirect_to admin_products_path, flash: {danger: t('view.messages.not_found')} unless @product
  end
  def product_params
    params.require(:product).permit :name, :price, :note, :sale, :description, :category_id, :status, :image, :short_desc,
                                    product_images_attributes: [:id, :picture, :product_id]
  end
end