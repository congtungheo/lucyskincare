class Backend::CategoriesController < Backend::BaseController
  authorize_resource
  before_action :find_category, only: [:edit, :update, :destroy]

  def index
    @categories = Category.active.page(params[:page]).per(30)
    @breadcrumbs = {'categories' => admin_categories_path, t('view.layout.list') => nil}
  end

  def new
    @breadcrumbs = {'categories' => admin_products_path, t('view.button.new') => nil}
    @category = Category.new
    @categories = Category.parent_categories.active
  end

  def create
    @categories = Category.new category_params
    if @categories.save
      respond_to do |format|
        format.html {redirect_to admin_categories_path, notice: t('view.messages.created_success', model_name: 'Category')}
      end
    else
      @breadcrumbs = {t('view.managers.managers') => admin_categories_path, t('view.button.new') => nil}
      render :new
    end
  end

  def edit
    @breadcrumbs = {'categories' => admin_categories_path, t('view.button.edit') => nil}
    @categories = Category.parent_categories.active
  end

  def update
    if @category.update_attributes category_params
      respond_to do |format|
        format.html {redirect_to admin_categories_path, notice: t('view.messages.updated_success', model_name: 'Category')}
      end
    else
      @breadcrumbs = {'categories' => admin_categories_path, t('view.button.edit') => nil}
      render :edit
    end
  end

  def destroy
    if @category.destroy
      flash[:success] = t('view.messages.deleted_success', model_name: 'Category')
    else
      flash[:danger] = @category.errors.full_messages
    end

    redirect_to admin_categories_path
  end

  private

  def find_category
    @category = Category.find_by_id params[:id]
    redirect_to admin_categories_path, flash: {danger: t('view.messages.not_found')} unless @category
  end
  def category_params
    params.require(:category).permit :name, :status, :parent_id
  end
end