class Frontend::HomeController < Frontend::BaseController
  def index
  	@products = Product.active.limit 6	
  	@parent_categories = Category.active.parent_categories.includes(:categories)
  end
end
