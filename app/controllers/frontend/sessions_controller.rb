class Frontend::SessionsController < Devise::SessionsController
  layout 'devise'

  def create
    super do |resource|
      unless resource.is_actived
        sign_out resource
        flash[:danger] = t('view.messages.account_deactivated')
        flash.delete(:notice)
        redirect_to new_user_session_url
        return
      end
    end
  end
end
