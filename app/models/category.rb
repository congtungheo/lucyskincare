class Category < ApplicationRecord
  has_many :products
  has_many :categories, :foreign_key => "parent_id" 
  belongs_to :parent, :class_name => 'Category'

  enum status: [:inactive, :active ]

  scope :parent_categories, -> {where parent_id: 0}
  scope :child_categories, -> { where('parent_id > 0') }
end
