class Product < ApplicationRecord
  acts_as_paranoid

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  belongs_to :category
  has_many :product_images, dependent: :destroy

  accepts_nested_attributes_for :product_images, allow_destroy: true
  enum status: [:inactive, :active ]
end
