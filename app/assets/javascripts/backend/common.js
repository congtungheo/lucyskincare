$(document).on("turbolinks:load", function () {
  $(document).ready(function () {
    $("body").click(function () {
      $(".alert").alert("close");
    });
    $("#edit-admin .star-password").remove();
    $("#edit-admin .star-password-confirmation").remove();

    $(".listing-table").on("click", ".remove", function () {
      $('#btn-confirm-delete').attr('href', $(this).data("url"));
    });

    icheck();
  });

  function icheck() {
    if ($(".icheck-me").length > 0) {
      $(".icheck-me").each(function () {
        var $el = $(this);
        var skin = ($el.attr('data-skin') !== undefined) ? "_" + $el.attr('data-skin') : "",
          color = ($el.attr('data-color') !== undefined) ? "-" + $el.attr('data-color') : "";
        var opt = {
          checkboxClass: 'icheckbox' + skin + color,
          radioClass: 'iradio' + skin + color,
        };
        $el.iCheck(opt);
      });
    }
  }

  $(document).on('change', '#product_image', function() {
    var src = document.getElementById("product_image");
    console.log(src);
    var target = document.getElementById("image_product");
    showImage(src, target);
  });
  function showImage(src, target) {
    var fr = new FileReader();
    fr.onload = function(){
        target.src = fr.result;
    }
    fr.readAsDataURL(src.files[0]);
  }
  $(document).on('change', '.image-other', function() {
    var src = $(this);
    var target = $(this).parent('.nested_product_product_images').find('img')[0];
    showImage(src.get(0), target);
  })  
});
