class AddImageToProductImages < ActiveRecord::Migration[5.2]
  def change
  	add_attachment :product_images, :picture
  end
end
