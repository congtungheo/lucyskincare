class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.text :address
      t.string :phone_number
      t.integer :user_id

      t.timestamps
    end
  end
end
