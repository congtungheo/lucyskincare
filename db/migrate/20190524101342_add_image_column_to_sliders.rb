class AddImageColumnToSliders < ActiveRecord::Migration[5.2]
  def up
    add_attachment :sliders, :image
  end

  def down
    remove_attachment :sliders, :image
  end
end
