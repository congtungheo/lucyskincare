class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :price
      t.integer :sale, default: 0
      t.text :description
      t.text :note
      t.integer :category_id

      t.integer :status, default: 0

      t.timestamps
    end
  end
end
