Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: {
    sessions: 'frontend/sessions',
    passwords: 'frontend/passwords'
  }
  devise_for :admin, path: 'admin', controllers: {
    sessions: 'backend/sessions',
    passwords: 'backend/passwords'
  }

  scope :admin, as: :admin do
    scope module: :backend do
      root 'home#index'
      resources :admins, except: :show
      resources :products
      resources :categories
      resources :users
      resources :sliders
    end
  end

  scope module: :frontend do
    root 'home#index'
  end
end
